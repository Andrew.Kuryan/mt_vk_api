buildscript {

    repositories {
        mavenLocal()
        mavenCentral()
    }

    dependencies {
        classpath("org.junit.platform:junit-platform-gradle-plugin:1.1.0")
        classpath("org.junit.platform:junit-platform-launcher:1.1.0")
    }
}

plugins {
    java
}

group = "org.mt"
version = "2.0.1"

repositories {
    mavenCentral()
}

val junit_version = "5.1.0"

dependencies {
    implementation("org.glassfish:javax.json:1.1.4")

    testImplementation("org.junit.jupiter:junit-jupiter-api:$junit_version")
    testRuntime("org.junit.jupiter:junit-jupiter-engine:$junit_version")
}

java {
    tasks.withType<Jar>().all {
        configurations["compileClasspath"].forEach { file: File ->
            from(zipTree(file.absoluteFile))
        }
    }
}

tasks.register<Exec>("publish") {
    dependsOn("jar")
    commandLine("mvn", "install:install-file", "-Dfile=build/libs/MT_VK_API-$version.jar",
            "-DgroupId=$group", "-DartifactId=vk", "-Dversion=$version", "-Dpackaging=jar")
}