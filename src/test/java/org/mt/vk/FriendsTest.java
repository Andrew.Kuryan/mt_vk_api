package org.mt.vk;

import org.mt.vk.user.LightUser;
import org.mt.vk.userLists.Friends;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.FileReader;
import java.util.ArrayList;

public class FriendsTest extends Friends {

    public FriendsTest(LightUser u){
        super(u);
    }

    public ArrayList<LightUser> loadUsersList(String id) {
        ArrayList<LightUser> ans = new ArrayList<>();
        int count;
        try{
            FileReader fr = new FileReader("src/MT_VK_API.tests/friends.test");
            JsonReader jr = Json.createReader(fr);
            JsonObject object = jr.readObject();
            jr.close();
            object = object.getJsonObject("response");
            JsonArray a = object.getJsonArray("items");
            count = object.getInt("count");
            for(int i = 0; i < count; i++)
                ans.add(new LightUser(a.getJsonObject(i)));
        }catch(Exception e){
            e.printStackTrace();
        }
        return ans;
    }
}
