package org.mt.vk;

import org.mt.vk.user.LightUser;
import org.mt.vk.user.User;

import java.util.Date;

public class MT_VK_APT_Test {

    static int id;

    public static void fullLocalTest(){
        LightUserTest lut = new LightUserTest();
        System.out.println();
        printLightUserFields(lut);
        System.out.println("----------------------------------");
        UserTest ut = new UserTest();
        printUserFields(ut);
        System.out.println("----------------------------------");
    }

    public static void fullRealTest(String screen_name){
        lightUserTest(screen_name);
        userTest(screen_name);
    }

    public static void printLightUserFields(LightUser u){
        System.out.println("> > Light User "+u.ID);
        System.out.println("First Name: "+u.first_name);
        System.out.println("Last Name: "+u.last_name);
        System.out.println("Domain: "+u.domain);
        System.out.println("Online: "+u.online);
        System.out.println("Last seen: "+new Date(u.last_seen * 1000));
        System.out.println("Small ava address: "+u.avaSmall);
    }

    public static void lightUserTest(String screen_name){
        long start = System.currentTimeMillis();
        LightUser lu = new LightUser(screen_name);
        try {
            lu.loadLightUser();
        }catch (RequestException exc){
            exc.printStackTrace();
        }
        printLightUserFields(lu);
        System.out.println();
        System.out.println("Time of light user load: "+(System.currentTimeMillis()-start)+" ms");
        System.out.println("----------------------------------");
    }

    public static void printUserFields(User u){
        System.out.println("> > User "+u.ID);
        System.out.println("> Inherited:");
        System.out.println("To String: "+u.toString());
        System.out.println("Domain: "+u.domain);
        System.out.println("Online: "+u.online);
        System.out.println("Last seen: "+new Date(u.last_seen * 1000));
        System.out.println("Small ava address: "+u.avaSmall);
        System.out.println();

        System.out.println("Status: "+u.status);
        System.out.println("Sex: "+u.sex);
        System.out.println("Blacklisted: "+u.blacklisted);
        System.out.println("Birthday: "+u.bdate);
        System.out.println("Country: "+u.country);
        System.out.println("City: "+u.city);
        System.out.println("Occupation type: "+u.occupType+" - "+u.getOccupType());
        System.out.println("Occupation name: "+u.occupName);
        System.out.println("Site: "+u.site);
        System.out.println("Activities: "+u.activities);
        System.out.println("About: "+u.about);
        System.out.println("Num of friends: "+u.numFriends);
        System.out.println("Num of follows: "+u.numFollows);
        System.out.println("Num of groups: "+u.numGroups);
        System.out.println("Big ava address: "+u.avaBig);
    }

    public static void userTest(String screen_name){
        long start = System.currentTimeMillis();
        User u = new User(screen_name);
        try{
            u.loadUser();
        }catch (RequestException exc){
            exc.printStackTrace();
        }
        id = u.ID;
        printUserFields(u);
        System.out.println();
        System.out.println("Time of user load: "+(System.currentTimeMillis()-start)+" ms");
        System.out.println("----------------------------------");
    }

}
