package org.mt.vk;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mt.vk.RequestException;
import org.mt.vk.authorization.Authorization;
import org.mt.vk.user.LightUser;
import org.mt.vk.userLists.Follows;
import org.mt.vk.userLists.Friends;

import java.util.ArrayList;

class Main {

    @BeforeAll
    static void initAccount() {
        Authorization.setCacheDirectory("src/test/resources/org/mt/vk/cache/");
        Authorization.addAccount(465449158, "&access_token=715f506fe8841a825dd25e272d8e1be9fb6ddcb1fda7d3a4ab4d6d2a59c8ef970d66559ba1f1e68b6d80d");
        Authorization.setCurrent(465449158);
    }

    @Test
    void testFriends() {
        LightUser u = new LightUser("fekz115");
        try{
            u.loadLightUser();
            Friends friends = new Friends(u);
            ArrayList<LightUser> al = friends.loadUsersList();
            System.out.println(al);
        }catch (RequestException exc){
            exc.printStackTrace();
        }
    }

    @Test
    void testFollows() {
        LightUser u = new LightUser("id321727316");
        try{
            u.loadLightUser();
            Follows friends = new Follows(u);
            ArrayList<LightUser> al = friends.loadUsersList();
            System.out.println(al);
        }catch (RequestException exc){
            exc.printStackTrace();
        }
    }
}