package org.mt.vk;

import org.mt.vk.user.User;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.*;

public class UserTest extends User{

    public UserTest(){
        try{
            FileReader fr = new FileReader("src/MT_VK_API.tests/user.test");
            JsonReader jr = Json.createReader(fr);
            JsonObject object = jr.readObject();
            jr.close();
            super.getInfo(object.getJsonArray("response").getJsonObject(0));
        }catch (IOException exc){
            exc.printStackTrace();
        }
    }
}
