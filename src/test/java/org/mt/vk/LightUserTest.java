package org.mt.vk;

import org.mt.vk.user.LightUser;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.FileReader;
import java.io.IOException;

public class LightUserTest extends LightUser {

    public LightUserTest(){
        try{
            FileReader fr = new FileReader("src/MT_VK_API.tests/lightuser.test");
            JsonReader jr = Json.createReader(fr);
            JsonObject object = jr.readObject();
            jr.close();
            super.getLightInfo(object.getJsonArray("response").getJsonObject(0));
        }catch (IOException exc){
            exc.printStackTrace();
        }
    }
}
