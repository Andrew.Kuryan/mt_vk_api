package org.mt.vk;

public class RequestException extends Exception{

    private String err;
    private int errorCode;

    public RequestException(String err, int errorCode){
        this.err = err;
        this.errorCode = errorCode;
    }

    public static void checkException(int errorCode) throws RequestException{
        switch (errorCode){
            case 5:
                throw new RequestException("Ошибка авторизации", 5);
            case 6:
                throw new RequestException("Слишком много запросов в секунду", 6);
            case 15:
            case 7:
                throw new RequestException("Недостаточно прав для выполнения действия", 7);
            case 10:
                throw new RequestException("Внутренняя ошибка сервера", 10);
            case 14:
                throw new RequestException("Требуется ввод кода с картинки", 14);
            case 17:
                throw new RequestException("Требуется валидация", 17);
            case 18:
                throw new RequestException("Страница удалена или заблокирована", 18);
            case 30:
                throw new RequestException("Приватный профиль", 30);
            case 113:
                throw new RequestException("Неверный идентификатор", 113);
            case 500:
                throw new RequestException("Отсутствует интернет-соединение", 500);
            case 1:
            default:
                throw new RequestException("Неизвестная ошибка", 1);
        }
    }

    public RequestException(int errorCode){
        this.errorCode = errorCode;
        switch (errorCode){
            case 5:
                err = "Ошибка авторизации";
                break;
            case 6:
                err = "Слишком много запросов в секунду";
                break;
            case 15:
            case 7:
                err = "Недостаточно прав для выполнения действия";
                break;
            case 10:
                err = "Внутренняя ошибка сервера";
                break;
            case 14:
                err = "Требуется ввод кода с картинки";
                break;
            case 17:
                err = "Требуется валидация";
                break;
            case 18:
                err = "Страница удалена или заблокирована";
                break;
            case 30:
                err = "Приватный профиль";
                break;
            case 113:
                err = "Неверный идентификатор";
                break;
            case 500:
                err = "Отсутствует интернет-соединение";
                break;
            case 1:
            default:
                err = "Неизвестная ошибка";
        }
    }

    public int getErrorCode(){
        return errorCode;
    }

    @Override
    public String toString() {
        return err;
    }
}
