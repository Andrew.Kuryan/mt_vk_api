package org.mt.vk.userLists;

import org.mt.vk.RequestException;
import org.mt.vk.authorization.Authorization;
import org.mt.vk.user.LightUser;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 * Created by andrew on 08.07.18.
 */

public class Follows implements UserLoaderable {

    private LightUser user;
    private ArrayList<LightUser> ans;

    public Follows(LightUser user) {
        this.user = user;
    }

    public ArrayList<LightUser> getUsersList(){
        return ans;
    }

    @Override
    public String getName(){
        return "Followers";
    }

    @Override
    public ArrayList<LightUser> loadUsersList() throws RequestException {
        ans = new ArrayList<>();
        int count, i = 0, j = 0;
        try{
            JsonObject object;
            String url1 = "https://api.vk.com/method/users.getFollowers?user_id="
                        .concat(Integer.toString(user.ID))
                        .concat(Authorization.curAcc.token)
                        .concat("&count=0&v=5.8");
            URL address = new URL(url1);
            InputStream is = address.openStream();
            JsonReader jsonReader = Json.createReader(is);
            object = jsonReader.readObject();
            jsonReader.close();
            if (object.containsKey("error"))
                RequestException.checkException(object.getJsonObject("error").getInt("error_code"));
            object = object.getJsonObject("response");
            count = object.getInt("count");
            while(j < count) {
                String url2 = "https://api.vk.com/method/users.getFollowers?user_id="
                        .concat(Integer.toString(user.ID))
                        .concat(Authorization.curAcc.token)
                        .concat("&offset=").concat(Integer.toString(j))
                        .concat("&fields=").concat(LightUser.fields)
                        .concat("&v=5.8");
                address = new URL(url2);
                is = address.openStream();
                jsonReader = Json.createReader(is);
                object = jsonReader.readObject();
                jsonReader.close();
                if (object.containsKey("error"))
                    RequestException.checkException(object.getJsonObject("error").getInt("error_code"));
                object = object.getJsonObject("response");
                JsonArray a = object.getJsonArray("items");
                for (i = 0; i < a.size(); i++) {
                    ans.add(new LightUser(a.getJsonObject(i)));
                }
                j+=i;
            }
        } catch (RequestException re){
            if (re.getErrorCode() == 6)
                return loadUsersList();
            else
                throw re;
        } catch (NullPointerException np){
            throw new RequestException(5);
        } catch (IOException e) {
            throw new RequestException(500);
        }
        return ans;
    }
}
