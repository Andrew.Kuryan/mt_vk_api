package org.mt.vk.userLists;

import org.mt.vk.RequestException;
import org.mt.vk.user.LightUser;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by andrew on 08.07.18.
 */

public interface UserLoaderable extends Serializable{

    String getName();

    ArrayList<LightUser> loadUsersList() throws RequestException;
}
