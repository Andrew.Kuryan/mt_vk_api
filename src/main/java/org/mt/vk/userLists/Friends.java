package org.mt.vk.userLists;

import org.mt.vk.RequestException;
import org.mt.vk.authorization.Authorization;
import org.mt.vk.user.LightUser;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 * Created by andrew on 08.07.18.
 */

public class Friends implements UserLoaderable {

    private LightUser user;
    private ArrayList<LightUser> ans;

    public Friends(LightUser user){
        this.user = user;
    }

    public ArrayList<LightUser> getUsersList(){
        return ans;
    }

    @Override
    public String getName(){
        return "Friends";
    }

    @Override
    public ArrayList<LightUser> loadUsersList() throws RequestException{
        ans = new ArrayList<>();
        int count;
        final String url1 = "https://api.vk.com/method/friends.get?user_id="
                .concat(Integer.toString(user.ID))
                .concat(Authorization.curAcc.token)
                .concat("&fields=").concat(LightUser.fields)
                .concat("&v=5.8");

        final String url2 = "https://api.vk.com/method/friends.get?user_id="
                .concat(Integer.toString(user.ID))
                .concat("&offset=5000")
                .concat(Authorization.curAcc.token)
                .concat("&fields=").concat(LightUser.fields)
                .concat("&v=5.8");
        try{
            JsonObject object;
            URL address = new URL(url1);

            HttpURLConnection httpConn = (HttpURLConnection) address.openConnection();
            httpConn.setConnectTimeout(10*1000);
            httpConn.setReadTimeout(10*1000);

            InputStream is = httpConn.getInputStream();
            JsonReader jsonReader = Json.createReader(is);
            object = jsonReader.readObject();
            jsonReader.close();
            if (object.containsKey("error"))
                RequestException.checkException(object.getJsonObject("error").getInt("error_code"));
            object = object.getJsonObject("response");
            JsonArray a = object.getJsonArray("items");
            count = object.getInt("count");
            for(int i = 0; i < (count <= 5000 ? count : 5000); i++){
                ans.add(new LightUser(a.getJsonObject(i)));
            }
            if(count > 5000){
                address = new URL(url2);

                httpConn = (HttpURLConnection) address.openConnection();
                httpConn.setConnectTimeout(10*1000);
                httpConn.setReadTimeout(10*1000);

                is = httpConn.getInputStream();
                jsonReader = Json.createReader(is);
                object = jsonReader.readObject();
                jsonReader.close();
                object = object.getJsonObject("response");
                a = object.getJsonArray("items");
                for(int i = 5000; i < count; i++){
                    ans.add(new LightUser(a.getJsonObject(i)));
                }
            }
        } catch (RequestException re){
            if (re.getErrorCode() == 6)
                return loadUsersList();
            else
                throw re;
        } catch (NullPointerException np){
            throw new RequestException(5);
        } catch (IOException e) {
            throw new RequestException(500);
        }
        return ans;
    }
}
