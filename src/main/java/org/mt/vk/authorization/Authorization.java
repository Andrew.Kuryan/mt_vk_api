package org.mt.vk.authorization;

import org.mt.vk.RequestException;

import javax.json.*;
import java.io.*;
import java.util.HashMap;
import java.util.Set;

public class Authorization {

    public static HashMap<Account, JsonObject> accMap = new HashMap<>();
    public static Account curAcc;
    private static String fileName = "auth.json";

    public static void setCacheDirectory(String path){
        System.out.println("Cache dir changed to: " + path);
        fileName = path.concat(fileName);
    }

    public static Set<Account> getAll(){
        return accMap.keySet();
    }

    public static void checkForAccounts(){
        try{
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            JsonReader reader = Json.createReader(br);
            JsonArray array = reader.readArray();
            for (int i=0; i<array.size(); i++){
                JsonObject jAcc = array.getJsonObject(i);
                Account account = new Account(jAcc.getInt("id"), jAcc.getString("token"));
                if (jAcc.containsKey("first_name"))
                    account.first_name = jAcc.getString("first_name");
                if (jAcc.containsKey("last_name"))
                    account.last_name = jAcc.getString("last_name");
                if (jAcc.containsKey("ava_small"))
                    account.avaSmall = jAcc.getString("ava_small");
                accMap.put(account, jAcc);
            }
            reader.close();
        }catch (FileNotFoundException exc){
            System.err.println("Существующих аккаунтов не найдено");
        }
    }

    public static boolean addAccount(int ID, String token){
        Account account = new Account(ID, token);
        JsonObject jAcc = Json.createObjectBuilder().add("id", account.ID)
                                                    .add("token", account.token)
                                                    .build();
        for (Account acc : accMap.keySet()){
            if (acc.ID == account.ID)
                return false;
        }
        accMap.put(account, jAcc);
        curAcc = account;
        rewriteAccounts();
        return true;
    }

    public static void loadAll() throws RequestException {
        for (Account account : accMap.keySet()){
            loadAccount(account.ID, false);
        }
        rewriteAccounts();
    }

    public static void loadAccount(int ID, boolean needRewrite) throws RequestException{
        for (Account account : accMap.keySet()){
            if (account.ID == ID){
                account.load();
                JsonObject object = accMap.get(account);
                object = Json.createObjectBuilder().add("id", object.getInt("id"))
                        .add("token", object.getString("token"))
                        .add("first_name", account.first_name)
                        .add("last_name", account.last_name)
                        .add("ava_small", account.avaSmall)
                        .build();
                accMap.replace(account, object);
                if (needRewrite)
                    rewriteAccounts();
            }
        }
    }

    public static void deleteAccount(int ID){
        Account toDelete = null;
        for (Account account : accMap.keySet()){
            if (account.ID == ID) {
                toDelete = account;
                break;
            }
        }
        if (toDelete != null) {
            accMap.remove(toDelete);
            rewriteAccounts();
        }
    }

    public static void setCurrent(int ID){
        for (Account account : accMap.keySet()){
            if (account.ID == ID)
                curAcc = account;
        }
    }

    public static void rewriteAccounts(){
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
            JsonWriter writer = Json.createWriter(bw);
            JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
            for (JsonObject obj : accMap.values())
                arrayBuilder.add(obj);
            writer.write(arrayBuilder.build());
            writer.close();
        }catch (IOException exc){
            exc.printStackTrace();
        }
    }
}