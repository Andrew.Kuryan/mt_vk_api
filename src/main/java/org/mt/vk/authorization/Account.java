package org.mt.vk.authorization;

import org.mt.vk.RequestException;
import org.mt.vk.user.LightUser;

public class Account{

    public int ID;
    public String token = "",
            first_name = "",
            last_name = "",
            avaSmall = "";

    Account(int ID, String token){
        this.ID = ID;
        this.token = token;
    }

    public void load() throws RequestException {
        LightUser u = new LightUser(Integer.toString(ID));
        u.loadLightUser();
        this.first_name = u.first_name;
        this.last_name = u.last_name;
        this.avaSmall = u.avaSmall;
    }
}
