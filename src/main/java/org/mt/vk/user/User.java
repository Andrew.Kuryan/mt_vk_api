package org.mt.vk.user;

import org.mt.vk.authorization.Authorization;
import org.mt.vk.RequestException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

public class User extends LightUser {

    public static final String fields = "photo_max,"
                                        .concat("status,")
                                        .concat("bdate,")
                                        .concat("country,")
                                        .concat("city,")
                                        .concat("occupation,")
                                        .concat("site,")
                                        .concat("sex,")
                                        .concat("blacklisted,")
                                        .concat("activities,")
                                        .concat("about,")
                                        .concat("counters");

    public String avaBig = "",
                  status = "",
                  bdate = "",
                  country = "",
                  city = "",
                  occupType = "",
                  occupName = "",
                  site = "",
                  activities = "",
                  about = "";
    public int sex,
               blacklisted,
               numFriends = -1,
               numFollows = -1,
               numGroups = -1;

    public String getOccupType(){
        switch (occupType){
            case "school":
                return "Школа";
            case "university":
                return "Университет";
            case "work":
                return "Место работы";
        }
        return "";
    }

    public User(){}

    public User(String screen_name) {
        super(screen_name);
    }

    public void loadUser() throws RequestException{
        try {
            JsonObject object;
            String url = "https://api.vk.com/method/users.get?user_ids="
                    .concat(screen_name)
                    .concat("&fields=")
                    .concat(LightUser.fields).concat(",").concat(fields)
                    .concat(Authorization.curAcc.token)
                    .concat("&v=5.8");

            URL address = new URL(url);

            HttpURLConnection httpConn = (HttpURLConnection) address.openConnection();
            httpConn.setConnectTimeout(10*1000);
            httpConn.setReadTimeout(10*1000);

            InputStream is = httpConn.getInputStream();
            JsonReader jsonReader = Json.createReader(is);
            object = jsonReader.readObject();
            jsonReader.close();
            is.close();
            if (object.containsKey("error"))
                RequestException.checkException(object.getJsonObject("error").getInt("error_code"));
            getInfo(object.getJsonArray("response").getJsonObject(0));
        }
        catch (RequestException re){
            if (re.getErrorCode() == 6)
                loadUser();
            else
                throw re;
        }
        catch (NullPointerException np){
            throw new RequestException(5);
        }
        catch (IOException e) {
            throw new RequestException(500);
        }
    }

    public void getInfo(JsonObject object){
        getLightInfo(object);
        if (object.containsKey("photo_max"))
            avaBig = object.getString("photo_max");
        if (object.containsKey("status"))
            status = object.getString("status");
        if(object.containsKey("bdate"))
            bdate = object.getString("bdate");
        if(object.containsKey("country"))
            country = object.getJsonObject("country").getString("title");
        if(object.containsKey("city"))
            city = object.getJsonObject("city").getString("title");
        if(object.containsKey("occupation")){
            occupType = object.getJsonObject("occupation").getString("type");
            if(object.getJsonObject("occupation").containsKey("name"))
                occupName = object.getJsonObject("occupation").getString("name");
        }
        if(object.containsKey("site"))
            site = object.getString("site");
        if (object.containsKey("sex"))
            sex = object.getInt("sex");
        if (object.containsKey("blacklisted"))
            blacklisted = object.getInt("blacklisted");
        if (object.containsKey("counters")) {
            if (object.getJsonObject("counters").containsKey("friends"))
                numFriends = object.getJsonObject("counters").getInt("friends");
            if (object.getJsonObject("counters").containsKey("followers"))
                numFollows = object.getJsonObject("counters").getInt("followers");
            if (object.getJsonObject("counters").containsKey("groups"))
                numGroups = object.getJsonObject("counters").getInt("groups");
        }
        if(object.containsKey("activities"))
            activities = object.getString("activities");
        if(object.containsKey("about"))
            about = object.getString("about");
    }
}
