package org.mt.vk.user;

import org.mt.vk.authorization.Authorization;
import org.mt.vk.RequestException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 * Created by andrew on 24.06.18.
 */

public class LightUser{

    public static final String fields = "photo_100,"
                                        .concat("online,")
                                        .concat("last_seen,")
                                        .concat("domain");
    public int ID;

    public String screen_name,
                  first_name = "",
                  last_name = "",
                  avaSmall = "",
                  domain = "";
    public int online;
    public long last_seen;

    public LightUser(){}

    public LightUser(int ID){
        this.ID = ID;
    }

    public LightUser(String screen_name){
        this.screen_name = screen_name;
    }

    public void loadLightUser() throws RequestException{
        try {
            JsonObject object;
            String url = "https://api.vk.com/method/users.get?user_ids="
                    .concat(screen_name)
                    .concat("&fields=")
                    .concat(fields)
                    .concat(Authorization.curAcc.token)
                    .concat("&v=5.8");
            URL address = new URL(url);

            HttpURLConnection httpConn = (HttpURLConnection) address.openConnection();
            httpConn.setConnectTimeout(10*1000);
            httpConn.setReadTimeout(10*1000);

            InputStream is = httpConn.getInputStream();
            JsonReader jsonReader = Json.createReader(is);
            object = jsonReader.readObject();
            jsonReader.close();
            is.close();
            if (object.containsKey("error"))
                RequestException.checkException(object.getJsonObject("error").getInt("error_code"));
            getLightInfo(object.getJsonArray("response").getJsonObject(0));
        }
        catch (RequestException re){
            if (re.getErrorCode() == 6)
                loadLightUser();
            else
                throw re;
        }
        catch (NullPointerException np){
            throw new RequestException(5);
        }
        catch (IOException exc) {
            throw new RequestException(500);
        }
    }

    public void getLightInfo(JsonObject object){
        ID = object.getInt("id");
        first_name = object.getString("first_name");
        last_name = object.getString("last_name");
        if (object.containsKey("photo_100"))
            avaSmall = object.getString("photo_100");
        if (object.containsKey("domain"))
            domain = object.getString("domain");
        if (object.containsKey("online"))
            online = object.getInt("online");
        if (object.containsKey("last_seen"))
            last_seen = object.getJsonObject("last_seen").getInt("time");
    }

    public LightUser(JsonObject object){
        ID = object.getInt("id");
        first_name = object.getString("first_name");
        last_name = object.getString("last_name");
        if (object.containsKey("photo_100"))
            avaSmall = object.getString("photo_100");
        if (object.containsKey("domain"))
            domain = object.getString("domain");
        if (object.containsKey("online"))
            online = object.getInt("online");
        if (object.containsKey("last_seen"))
            last_seen = object.getJsonObject("last_seen").getInt("time");
    }

    public String toString() {
        return first_name + " " + last_name;
    }
}
